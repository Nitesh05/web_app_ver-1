import api from '../../../../app/ApiConfig';
import { Apis } from '../../../../config';
import { NotificationManager } from 'react-notifications';

const getProductById = async (id) => {
    try {
        let result = await api.get(Apis.GetProductById+id);
        if (result.data.error) {
            NotificationManager.error(result.data.error);
            return null;
        }
        return result.data;
    } catch (error) {
        console.log(error);
        return null;
    }
};

const getAllProductList = async (id) => {
    try {
        // alert("id");
        let result = await api.get(Apis.GetAllProductListTest);
        if (result.data.error) {
            NotificationManager.error(result.data.error);
            return null;
        }
        return result.data;
    } catch (error) {
        console.log(error);
        return null;
    }
};

const getAllProductListByid = async (id) => {
    try {
        // alert(id)
        let result = await api.get(Apis.GetAllProductList+id);
        if (result.data.error) {
            NotificationManager.error(result.data.error);
            return null;
        }
        return result.data;
    } catch (error) {
        console.log(error);
        return null;
    }
};


const getProductByFilter = async (txt) => {
    try {
        let result = await api.get(Apis.GetProductByFilter+txt);
        if (result.data.error) {
            NotificationManager.error(result.data.error);
            return null;
        }
        return result.data;
    } catch (error) {
        console.log(error);
        return null;
    }
};

const getCategoryListByFilter = async (data) => {
    try {
        let result = await api.post(Apis.GetCategoryListByFilter,data);
        if (result.data.error) {
            NotificationManager.error(result.data.error);
            return null;
        }
        return result.data;
    } catch (error) {
        console.log(error);
        return null;
    }
};

const getProductBySubcategory = async (data) => {
    try {
        let result = await api.post(Apis.GetProductBySubcategory,data);
        if (result.data.error) {
            NotificationManager.error(result.data.error);
            return null;
        }
        return result.data;
    } catch (error) {
        console.log(error);
        return null;
    }
};

// Date:30/6/21
// Author:Nitesh
// Method:get
// Param: category_id
const getSubCategoryByCategory = async (id) => {
    try {
        // alert(id)
        let result = await api.get(Apis.GetSubCategoryByCategory+id);
        if (result.data.error) {
            NotificationManager.error(result.data.error);
            return null;
        }
        return result.data;
    } catch (error) {
        console.log(error);
        return null;
    }
};


const getSubCategoryById = async (id) => {
    try {
        let result = await api.get(Apis.GetSubCategoryById+id);
        if (result.data.error) {
            NotificationManager.error(result.data.error);
            return null;
        }
        return result.data;
    } catch (error) {
        console.log(error);
        return null;
    }
};

const getProductByCategoryId = async (id) => {
    try {
        let result = await api.get(Apis.GetAllProductList+id);
        if (result.data.error) {
            NotificationManager.error(result.data.error);
            return null;
        }
        return result.data;
    } catch (error) {
        console.log(error);
        return null;
    }
};
const getProductBySubCategoryId = async (id) => {
    try {
        let result = await api.get(Apis.GetProductBySubCategoryId+id);
        if (result.data.error) {
            NotificationManager.error(result.data.error);
            return null;
        }
        return result.data;
    } catch (error) {
        console.log(error);
        return null;
    }
};


export default {
    getAllProductList,
    getProductByFilter,
    getCategoryListByFilter,
    getProductBySubcategory,
    getAllProductListByid,

    getProductById,
    getProductByCategoryId,
    getSubCategoryByCategory,
    getSubCategoryById,
    getProductBySubCategoryId

};