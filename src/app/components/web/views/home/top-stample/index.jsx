import React, { useEffect, useState} from 'react'
import Slider from "react-slick";
import { dummy } from './dummy';
const Topstample =({categoryData})=> {
  
        var settings = {
            dots: false,
            infinite: false,
            speed: 500,
            slidesToShow: 5,
            slidesToScroll: 4,
            initialSlide: 0,
            responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  infinite: true,
                  dots: false
                }
              },
              {
                breakpoint: 768,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 3,
                  initialSlide: 3
                }
              },
              {
                breakpoint: 600,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2,
                  initialSlide: 2
                }
              },
              {
                breakpoint: 480,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
            ]
          };
          const product = dummy;

          const [productData, setProductData] = useState({});
          useEffect(() => {
            async function homadata() {
                setProductData(product)
            //   const res = await GetCategoryDetails.getAllCategoryList();
            //   if (res) {
            //     await setCategoryData(res.data);
            //   } else {
            //     console.log("error");
            //   }
            }
            homadata();
          }, []);
console.log(productData)
        // return (

        //     <div>
                 
        //          <section className="product-items-slider section-padding">
        //             <div className="container" id="header-category-bk">
        //                 <div className="section-header">
        //                     <span>For You</span>
        //                     <h5 className="heading-design-h5">Top Stample  {/* <span className="badge badge-primary">20% OFF</span> */}
        //                         <a className="float-right text-secondary" >View All</a>
        //                     </h5>
        //                 </div>
        //                 <Slider {...settings}>
        //                     <div className="item">
        //                         <div className="product">
        //                             <a href="single.html">
        //                                 <div className="product-header">
        //                                     <span className="badge badge-success">50% OFF</span>
        //                                     <img className="img-fluid" src="img/product/nehar-oil.jpeg" alt="product" />
        //                                     <span className="veg text-success mdi mdi-circle" />
        //                                 </div>
        //                                 <div className="product-body">
        //                                     <h5>Product Title Here</h5>
        //                                     <h6><strong><span className="mdi mdi-approval" /> Available in</strong> - 500 gm</h6>
        //                                 </div>
        //                                 <div className="product-footer">
        //                                     <button type="button" className="btn btn-secondary btn-sm float-right"><i className="mdi mdi-cart-outline" /> Add To Cart</button>
        //                                     <p className="offer-price mb-0">$450.99 <i className="mdi mdi-tag-outline" /><br /><span className="regular-price">$800.99</span></p>
        //                                 </div>
        //                             </a>
        //                         </div>
        //                     </div>
        //                     <div className="item">
        //                         <div className="product">
        //                             <a href="single.html">
        //                                 <div className="product-header">
        //                                     <span className="badge badge-success">50% OFF</span>
        //                                     <img className="img-fluid" src="img/product/new-product.jpeg" alt="product" />
        //                                     <span className="non-veg text-danger mdi mdi-circle" />
        //                                 </div>
        //                                 <div className="product-body">
        //                                     <h5>Product Title Here</h5>
        //                                     <h6><strong><span className="mdi mdi-approval" /> Available in</strong> - 500 gm</h6>
        //                                 </div>
        //                                 <div className="product-footer">
        //                                     <button type="button" className="btn btn-secondary btn-sm float-right"><i className="mdi mdi-cart-outline" /> Add To Cart</button>
        //                                     <p className="offer-price mb-0">$450.99 <i className="mdi mdi-tag-outline" /><br /><span className="regular-price">$800.99</span></p>
        //                                 </div>
        //                             </a>
        //                         </div>
        //                     </div>
        //                     <div className="item">
        //                         <div className="product">
        //                             <a href="single.html">
        //                                 <div className="product-header">
        //                                     <span className="badge badge-success">50% OFF</span>
        //                                     <img className="img-fluid" src="img/product/big-3.jpg" alt="product" />
        //                                     <span className="non-veg text-danger mdi mdi-circle" />
        //                                 </div>
        //                                 <div className="product-body">
        //                                     <h5>Product Title Here</h5>
        //                                     <h6><strong><span className="mdi mdi-approval" /> Available in</strong> - 500 gm</h6>
        //                                 </div>
        //                                 <div className="product-footer">
        //                                     <button type="button" className="btn btn-secondary btn-sm float-right"><i className="mdi mdi-cart-outline" /> Add To Cart</button>
        //                                     <p className="offer-price mb-0">$450.99 <i className="mdi mdi-tag-outline" /><br /><span className="regular-price">$800.99</span></p>
        //                                 </div>
        //                             </a>
        //                         </div>
        //                     </div>
        //                     <div className="item">
        //                         <div className="product">
        //                             <a href="single.html">
        //                                 <div className="product-header">
        //                                     <span className="badge badge-success">50% OFF</span>
        //                                     <img className="img-fluid" src="img/product/biscuit.jpeg" alt="product" />
        //                                     <span className="veg text-success mdi mdi-circle" />
        //                                 </div>
        //                                 <div className="product-body">
        //                                     <h5>Product Title Here</h5>
        //                                     <h6><strong><span className="mdi mdi-approval" /> Available in</strong> - 500 gm</h6>
        //                                 </div>
        //                                 <div className="product-footer">
        //                                     <button type="button" className="btn btn-secondary btn-sm float-right"><i className="mdi mdi-cart-outline" /> Add To Cart</button>
        //                                     <p className="offer-price mb-0">$450.99 <i className="mdi mdi-tag-outline" /><br /><span className="regular-price">$800.99</span></p>
        //                                 </div>
        //                             </a>
        //                         </div>
        //                     </div>
        //                     <div className="item">
        //                         <div className="product">
        //                             <a href="/">
        //                                 <div className="product-header">
        //                                     <span className="badge badge-success">50% OFF</span>
        //                                     <img className="img-fluid" src="img/product/img-1.jpg" alt="product" />
        //                                     <span className="veg text-success mdi mdi-circle" />
        //                                 </div>
        //                                 <div className="product-body">
        //                                     <h5>Product Title Here</h5>
        //                                     <h6><strong><span className="mdi mdi-approval" /> Available in</strong> - 500 gm</h6>
        //                                 </div>
        //                                 <div className="product-footer">
        //                                     <button type="button" className="btn btn-secondary btn-sm float-right"><i className="mdi mdi-cart-outline" /> Add To Cart</button>
        //                                     <p className="offer-price mb-0">$450.99 <i className="mdi mdi-tag-outline" /><br /><span className="regular-price">$800.99</span></p>
        //                                 </div>
        //                             </a>
        //                         </div>
        //                     </div>
        //                     <div className="item">
        //                         <div className="product">
        //                             <a href="single.html">
        //                                 <div className="product-header">
        //                                     <span className="badge badge-success">50% OFF</span>
        //                                     <img className="img-fluid" src="img/product/img-2.jpg" alt="product" />
        //                                     <span className="veg text-success mdi mdi-circle" />
        //                                 </div>
        //                                 <div className="product-body">
        //                                     <h5>Product Title Here</h5>
        //                                     <h6><strong><span className="mdi mdi-approval" /> Available in</strong> - 500 gm</h6>
        //                                 </div>
        //                                 <div className="product-footer">
        //                                     <button type="button" className="btn btn-secondary btn-sm float-right"><i className="mdi mdi-cart-outline" /> Add To Cart</button>
        //                                     <p className="offer-price mb-0">$450.99 <i className="mdi mdi-tag-outline" /><br /><span className="regular-price">$800.99</span></p>
        //                                 </div>
        //                             </a>
        //                         </div>
        //                     </div>
        //                 </Slider>
        //             </div>
        //         </section>
               
        //     </div>
        
        
        
        // )

        ////////////////////////////////////////////////////////////////////////////////

          return(
              <div>
                  {/* {console.log(JSON.stringify(categoryData))} */}
    
          {categoryData.length > 0 &&
            categoryData.map((value, key) => (
              <div className="category-item" key={key}>
                {/* {console.log(key,value)} */}
                <div className="category-item Category-details">
                  {/* <Link
                    to={{
                      pathname: `/shop/${value.category_id}`,
                      state:{category_name: value.category_name, category_image:value.category_image}
                    }}
                  >

                   

                   </Link> */}
                  
                    <img
                      className="image"
                      src={value.category_image}
                      alt={value.category_name}
                    />
                        <div class="overlay">
                             <div class="text">{value.category_name}</div>
                        </div>
                   

                  <h1>{value.category_name}</h1>
                  
                </div>

              </div>
            ))}
      
              </div>
          )








}
export default Topstample;
