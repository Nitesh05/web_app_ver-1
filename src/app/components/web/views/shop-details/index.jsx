import React, { Component } from "react";
import { GetProductDetails, GetCategoryDetails } from "../../../services";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { addToCart } from "../../../../store/actions/cartActions";
import { NotificationManager } from "react-notifications";
import "./index.css";
import Filterbycategory from "./Filtersbycategory";
import CircularProgress from "@material-ui/core/CircularProgress";
import Category from "../category";
import Slider from "react-slick";
import { updateLocale } from "moment";
import Sku from './Sku';

class Shopdetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      limit: 12,
      isloaded: false,
      categoryData: {},
      sku:[]
    };
  }
  async getProductByid() {
    let url = window.location.href.split("/");
    var lastSegment = url.pop() || url.pop();
    try {
      let p = await GetProductDetails.getProductByCategoryId(lastSegment);
      if (p.length > 0) {
        // console.log("getProductByCategoryId");
        this.setState({ list: p, isloaded: true });
        localStorage.setItem("p", JSON.stringify(p));
      } else {
        // console.log("getProductBySubCategoryId");
        p = await GetProductDetails.getProductBySubCategoryId(lastSegment);
        if (p) {
          this.setState({ list: p, isloaded: true });
          localStorage.setItem("p", JSON.stringify(p));
        }
      }

      ///////////////////////////////////////////////////
    } catch (e) {
      NotificationManager.error("Empty data in category", "Data");
    }
  }

  componentWillReceiveProps(nextProps) {
    this.getProductByid();
  }

  async componentDidMount() {
    // alert("componentDidMount");
    window.scrollTo(0, 0);
    try {
      const res = await GetCategoryDetails.getAllCategoryList();
      if (res) {
        this.setState({ categoryData: res.data });
      }
    } catch (e) {
      console.log(e);
    }
    this.getProductByid();
  }
  onLoadMore = (event) => {
    this.setState({ limit: this.state.limit + 6 });
  };
  handleChangeByCategory(value) {
    if (value) {
      this.setState({ isloaded: true, list: value.data });
    }
  }

 

  render() {
    var settings = {
      dots: false,
      infinite: true,
      autoplay: true,
      speed: 2000,
      autoplaySpeed: 2000,
      slidesToShow: 1,
      slidesToScroll: 1,
    };
    // console.log(window.history.state.state.id)
    let { list, isloaded, limit } = this.state;

    return (
      <div>
        <section className=" page-info section-padding border-bottom bg-white single-product-header-bk">
          <Category categoryData={this.state.categoryData} showImg={false} />
        </section>

        <Slider {...settings}>
          <div className="owl-item">
            <img
              src="https://picsum.photos/1000/250?random=2"
              alt="supermarket"
            />
          </div>
          <div className="owl-item">
            <img
              src="https://picsum.photos/1000/250?random=3"
              alt="supermarket"
            />
          </div>
          <div className="owl-item">
            <img
              src="https://picsum.photos/1000/250?random=4"
              alt="supermarket"
            />
          </div>
        </Slider>

        <section className=" page-info section-padding border-bottom bg-white single-product-header-bk">
          <Category subCategoryData={true} showImg={true} />
        </section>

        {/* All product */}
        <div className="all-product-grid">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="product-top-dt">
                  <Filterbycategory
                    onSelectFilterCategory={this.handleChangeByCategory.bind(
                      this
                    )}
                  />
                  <div className="product-sort">
                    <select className="form-control">
                      <option className="item" value={0}>
                        Sort by Products
                      </option>
                      <option className="item" value={1}>
                        Price - Low to High
                      </option>
                      <option className="item" value={2}>
                        Price - High to Low
                      </option>
                      <option className="item" value={3}>
                        Alphabetical
                      </option>
                      <option className="item" value={4}>
                        Saving - High to Low
                      </option>
                      <option className="item" value={5}>
                        Saving - Low to High
                      </option>
                      <option className="item" value={6}>
                        % Off - High to Low
                      </option>
                    </select>
                  </div>
                </div>
              </div>
              {/* <div className="col-lg-12">
                <div className="product-top-dt">
                  <div className="container">
                    <div className="row">
                      <div className="col-md-12">
                        <a href="/">
                          <strong>
                            <span className="mdi mdi-home" /> Home
                          </strong>
                        </a>
                        <span className="mdi mdi-chevron-right" />{" "}
                        <a href="/">
                          {window.history.state.state.category_name}
                        </a>
                        <span className="mdi mdi-chevron-right" />{" "}
                        <a href="#">Fruits</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div> */}
            </div>
          </div>
        </div>
        {/* End product */}
        {/* product section */}
        <section className="shop-list section-padding">
          {!isloaded ? (
            <div className="progress-bar-bk">
              <CircularProgress color="secondary" />
            </div>
          ) : (
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div className="row no-gutters">
                    {list.map((row, index) => (
                      <div key={index} className="col-md-3 col-sm-6 p-2">
                        <div className="item">
                          <div className="product">
                             <Link
                              to={{
                                pathname: `/product/${row.product_id}`,
                                state: row,
                              }}
                            >
                              <div className="product-header">
                                <span className="badge badge-success">
                                  {row.subcategory_name}
                                </span>
                                <img
                                  className="img-fluid"
                                  src={row.primary_image}
                                  alt="product"
                                />
                                <span className="veg text-success mdi mdi-circle" />
                              </div>
                              <div className="product-body">
                                <h5>{row.name}</h5>
                                <h6>
                                  <strong>
                                    <span className="mdi mdi-approval" />{" "}
                                    Available in 
                          {/* <p> {console.log( this.getSku(row.product_id))}</p> */}
                                {/* {JSON.stringify(this.state.sku)} */}
                                    
                       {/* { row.product_id} */}

                                  </strong>{" "}
                                  - {row.unitSize}
                                </h6>
                              </div>
                            </Link>
                            <Sku id={row.product_id}/>

                            <div className="product-footer">
                              <button
                                type="button"
                                className="btn btn-secondary btn-sm float-right"
                                onClick={() => this.props.addToCart(row)}
                              >
                                <i className="mdi mdi-cart-outline" /> Add To
                                Cart
                              </button>
                              <p className="offer-price mb-0">
                                &#x20B9;{row.product_name}{" "}
                                <i className="mdi mdi-tag-outline" />
                                <br />
                                <span className="regular-price">
                                  &#x20B9;{row.price}{" "}
                                </span>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>

                  <div class="more-product-btn">
                    <button
                      class="show-more-btn hover-btn"
                      onClick={this.onLoadMore}
                    >
                      Show More
                    </button>
                  </div>
                </div>
              </div>
            </div>
          )}
        </section>

        {/* end product section */}
      </div>
    );
  }
}
export default connect(null, { addToCart })(Shopdetails);
