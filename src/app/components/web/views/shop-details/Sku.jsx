import React, { useState, useEffect } from "react";

const Sku = (props) => {
  const [skuList, setSkuList] = useState([]);
//   const [selectValue, setSelectValue] = useState();

  useEffect(() => {
    fetch(`http://18.118.106.30:5000/v1/getskusbyproduct/${props.id}`)
      .then((res) => res.json())
      .then((data) => setSkuList([...data]));
  }, []);

  const handleChange = (e) => {
    alert(e.target.value);
    // setSelectValue(e.target.value);
  };

  return (
    <div>
      {skuList.map((data, i) => {
        return (
          <div key={i} style={{ border: "1px solid red" }}>
            <p>
              Available in :
              <select value={data.package_size} onChange={handleChange}>
                <option value={i}>{data.package_size}</option>
              </select>
            </p>
            <p>MRP : {data.product_mrp}</p>
            <p>Strick price : {data.strike_price}</p>
          </div>
        );
      })}
      {/* <h1>{JSON.stringify(skuList)}</h1> */}
    </div>
  );
};

export default Sku;
