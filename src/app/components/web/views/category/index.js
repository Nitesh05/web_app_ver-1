import React, { useState, useEffect } from "react";
import Slider from "react-slick";
import { Link } from "react-router-dom";
import { arr } from "./data";
import { GetProductDetails } from "../../../services";

const Category = (props) => {
  // const categoryData = data;
  const { categoryData, showImg = true, subCategoryData = false } = props;
  const [productData, setProductData] = useState({});
  // const[subCategoryData1,setSubCategoryData] = useState({});

  var settings = {
    // autoplay:true,
    // autoplaySpeed:100,
    nextArrow: <p></p>,
    prevArrow: <p></p>,
    arrows: true,
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 7,
    slidesToScroll: 2,
    initialSlide: 0.5,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 3,
          infinite: true,
          dots: false,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
    ],
  };

  // const [productsList, setProductsList] = useState([]);
  // const [isLoading, setisLoading] = useState([]);

  // useEffect(() => {
  //   fetch('http://18.118.106.30:5000/v1/getproduct')
  //     .then((res) => res.json())
  //     .then((data) => setProductsList([...data]))
  //     .then(setisLoading(false));
  // }, []);

  async function allProductData() {
    let url = window.location.href.split("/");
    let lastSegment = url.pop() || url.pop();
    try {
      if (lastSegment < 40) {
        let cat_Id = localStorage.getItem("category_id");
        if (cat_Id) {
          lastSegment = cat_Id;
        }
      }
      let res = await GetProductDetails.getSubCategoryByCategory(lastSegment);
      if (res) {
        await setProductData(res);
        await localStorage.setItem(
          "SubCategoryByCategory",
          JSON.stringify(res)
        );
      } else {
        console.log("error");
      }
    } catch (err) {
      console.log(err);
    }
  }
  useEffect(() => {
    allProductData();
  }, []);
  ///////////////////////////////////////////

  const SubCatagoryData = () => {
    return (
      <Slider {...settings}>
        {productData.length > 0 &&
          productData.map((value, key) => (
            <div className="item" key={key}>
              <div
                className="category-item"
                onClick={localStorage.setItem(
                  "category_id",
                  value.category.category_id
                )}
              >
                <Link
                  to={{
                    pathname: `/shop/${value.subcategory_id}`,
                    // state:{id:value.category.category_id}
                  }}
                >
                  <img
                    className="img-fluid"
                    src={value.subcategory_image}
                    alt={value.subcategory_name}
                  />
                  <h6> {value.subcategory_name}</h6>
                </Link>
              </div>
            </div>
          ))}
      </Slider>
    );
  };
  // console.log(JSON.stringify(data));

  return (
    <div style={{ background: "#eee" }}>
      <div className="container" id="header-category-bk">
        {!subCategoryData ? (
          <Slider {...settings}>
            {categoryData.length > 0 &&
              categoryData.map((value, key) => (
                <div className="item" key={key}>
                  <div
                    className="category-item"
                    onClick={() => window.location.reload()}
                  >
                    <Link
                      to={{
                        pathname: `/shop/${value.category_id}`,
                        state: {
                          category_name: value.category_name,
                          category_image: value.category_image,
                        },
                      }}
                    >
                      {showImg ? (
                        <div>
                          <img
                            className="image"
                            src={value.category_image}
                            alt={value.category_name}
                          />
                          <div class="overlay">
                            <div class="text">{value.category_name}</div>
                          </div>
                        </div>
                      ) : (
                        <h6 style={{ border: "2px solid red" }}>
                          {value.category_name}
                        </h6>
                      )}
                    </Link>
                  </div>
                </div>
              ))}
          </Slider>
        ) : (
          <SubCatagoryData />
        )}
      </div>
    </div>
  );
  // }
};
export default Category;
