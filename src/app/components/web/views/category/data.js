export const arr = [
    // { id: 2, name: "sumit" },
    // { id: 1, name: "amit" },
    // { id: 3, name: "rahul" },
    // { id: 4, name: "jay" },
    // { id: 2, name: "ra one" },
    // { id: 3, name: "alex" },
    // { id: 1, name: "devid" },
    // { id: 7, name: "sam" },
    // { id: 4, name: "jay" },
    // { id: 2, name: "ra one" },
    // { id: 3, name: "alex" },
    // { id: 1, name: "devid" },
    // { id: 7, name: "sam" },
    {
        "_id":"60d827eef78d031296966a72",
        "product_name":"Vrindavan Buffalo Milk",
        "product_alias":"Buffalo Milk - Blue",
        "primary_image":"http://13.58.204.168:3000/images/1624778691193.png",
        "secondary_image":[
        ],
        "cgst":12,
        "sgst":5,
        "igst":null,
        "status":1,
        "product_description":"<p>Fresh Buffalo Milk from Vrindavan Organic Farms</p>",
        "partner_id":3,
        "product_priority":200,
        "category":{
        "categoryName":"Dairy Items",
        "label":"Dairy Items",
        "category_id":44
        },
        "sub_category":{
        "subCategoryName":"Milk",
        "label":"Milk",
        "subCategory_id":9
        },
        "warehouse_id":1,
        "hub":{
        "hubName":"Koyal Grand",
        "hubId":1
        },
        "stocks_quantity":0,
        "mappedBy":{
        "value":"hub",
        "label":"Hub",
        "id":1,
        "state":0
        },
        "brand":{
        "brandName":"Vrindavan",
        "label":"Vrindavan",
        "brand_id":5
        },
        "subscription_type":"1",
        "isDelete":false,
        "created_at":"2021-06-27T07:25:34.565Z",
        "product_id":26,
        "__v":0
        },
        {
        "_id":"60d8297ef78d031296966a74",
        "product_name":"Vrindavan Natural Cow Milk",
        "product_alias":"A1 Milk - Green",
        "primary_image":"http://13.58.204.168:3000/images/1624779053380.png",
        "secondary_image":[
        ],
        "cgst":12,
        "sgst":5,
        "igst":null,
        "status":1,
        "product_description":"<p>Fresh Cow Milk from Virndavan Organic Farms</p>",
        "partner_id":3,
        "product_priority":201,
        "category":{
        "categoryName":"Dairy Items",
        "label":"Dairy Items",
        "category_id":44
        },
        "sub_category":{
        "subCategoryName":"Milk",
        "label":"Milk",
        "subCategory_id":9
        },
        "warehouse_id":1,
        "hub":{
        "hubName":"Koyal Grand",
        "hubId":1
        },
        "stocks_quantity":0,
        "mappedBy":{
        "value":"hub",
        "label":"Hub",
        "id":1,
        "state":0
        },
        "brand":{
        "brandName":"Vrindavan",
        "label":"Vrindavan",
        "brand_id":5
        },
        "subscription_type":"1",
        "isDelete":false,
        "created_at":"2021-06-27T07:32:14.064Z",
        "product_id":27,
        "__v":0
        },
        {
            "_id":"60d82cbef78d031296966a82",
            "product_name":"Vrindavan Farm Fresh Natural Curd",
            "product_alias":"A1 Curd - Green",
            "primary_image":"http://13.58.204.168:3000/images/1624779691527.png",
            "secondary_image":[
            ],
            "cgst":12,
            "sgst":5,
            "igst":null,
            "status":1,
            "product_description":"<p>Fresh Natural Curd from Vrindvan Organic Farms</p>",
            "partner_id":3,
            "product_priority":202,
            "category":{
            "categoryName":"Dairy Items",
            "label":"Dairy Items",
            "category_id":44
            },
            "sub_category":{
            "subCategoryName":"Curd",
            "label":"Curd",
            "subCategory_id":12
            },
            "warehouse_id":1,
            "hub":{
            "hubName":"Koyal Grand",
            "hubId":1
            },
            "stocks_quantity":0,
            "mappedBy":{
            "value":"hub",
            "label":"Hub",
            "id":1,
            "state":0
            },
            "brand":{
            "brandName":"Vrindavan",
            "label":"Vrindavan",
            "brand_id":5
            },
            "subscription_type":"2",
            "isDelete":false,
            "created_at":"2021-06-27T07:46:06.392Z",
            "product_id":31,
            "__v":0
            },
    ];